package com.cppba.service

import com.cppba.dto.UserDto
import com.cppba.mapper.UserMapper
import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService {

    @Autowired
    lateinit var userMapper: UserMapper

    fun getById(id: Long): UserDto {
        return userMapper.getById(id)
    }

    fun page(username: String?, pageNumber: Int, pageSize: Int): PageInfo<UserDto> {
        return PageHelper.startPage<UserDto>(pageNumber, pageSize).doSelectPageInfo<UserDto>({
            userMapper.page(username)
        })
    }
}
