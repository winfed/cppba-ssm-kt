package com.cppba.service

import com.cppba.entity.City
import com.cppba.mapper.CityMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author timingbar
 * @created 2017/7/6 0006 16:00
 */
@Service
class CityService{

    @Autowired
    lateinit var cityMapper: CityMapper

    fun findByParentId(parentId: Long): List<City> {
        return cityMapper.findByParentId(parentId)
    }

    fun insertSelective(city: City): Int{
        return cityMapper.insertSelective(city)
    }
}
