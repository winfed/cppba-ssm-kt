package com.cppba.controller

import com.cppba.dto.UserDto
import com.cppba.service.UserService
import com.github.pagehelper.PageInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/user")
class UserController{

    @Autowired
    lateinit var userService: UserService

    @RequestMapping("/getById")
    @ResponseBody
    fun getById(id: Long): UserDto{
        return userService.getById(id)
    }

    @RequestMapping("/page")
    @ResponseBody
    fun page(
            username: String? ,
            @RequestParam(name ="pageNumber",defaultValue = "1")pageNumber: Int ,
            @RequestParam(name ="pageSize",defaultValue = "2")pageSize: Int): PageInfo<UserDto>{
        return userService.page(username, pageNumber, pageSize)
    }
}
