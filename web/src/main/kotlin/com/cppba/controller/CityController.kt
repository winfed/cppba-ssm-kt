package com.cppba.controller

import com.cppba.entity.City
import com.cppba.service.CityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * @author timingbar
 * @created 2017/7/6 0006 16:00
 */
@Controller
@RequestMapping("/city")
class CityController {

	@Autowired
    lateinit var cityService: CityService

	@RequestMapping("/findByParentId")
	@ResponseBody
	fun findByParentId(parentId: Long): List<City> {
		return cityService.findByParentId(parentId)
	}

    @RequestMapping("/insert")
    @ResponseBody
    fun insert(): Int {
		var city = City()
		city.name = "new"
		city.parentId = 0
        return cityService.insertSelective(city)
    }
}
