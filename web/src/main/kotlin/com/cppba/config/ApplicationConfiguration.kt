package com.cppba.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

/**
 * @author zwf
 * @created 2017/11/07 0028 11:33
 */
@Configuration
class ApplicationConfiguration: WebMvcConfigurerAdapter() {
	/**
	 * 拦截器
	 * @param registry
	 */
    override fun addInterceptors(registry: InterceptorRegistry){
		// 拦截器
		// registry.addInterceptor(new OauthInterceptor()).addPathPatterns("/**")
	}

	/**
	 * 跨域配置
	 * @return
	 */
	@Bean
    fun corsFilter(): CorsFilter {
		var corsConfiguration = CorsConfiguration()
		corsConfiguration.addAllowedOrigin("*")
		corsConfiguration.addAllowedHeader("*")
		corsConfiguration.addAllowedMethod("*")
		var source  = UrlBasedCorsConfigurationSource()
		source.registerCorsConfiguration("/**", corsConfiguration)
		return CorsFilter(source)
	}
}
