package com.cppba.entity

import com.cppba.bean.BaseEntity
import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.Entity

@Entity
open class User(
        var username: String = "",
        // 屏蔽前端返回字段
        @JsonIgnore var password: String = "",
        var cityId: Long? = null,
        var sex: Int? = null
) : BaseEntity()
