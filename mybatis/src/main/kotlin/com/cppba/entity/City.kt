package com.cppba.entity

import com.cppba.bean.BaseEntity
import javax.persistence.Entity

/**
 * @author zwf
 * @created 2017/7/6 0006 16:04
 */
@Entity
class City(
        var name: String = "",
        var parentId: Int = 0
) : BaseEntity()
