package com.cppba.bean

import java.io.Serializable
import javax.persistence.MappedSuperclass

/**
 * @author zwf
 * @created 2017/6/28 0028 17:46
 */
@MappedSuperclass
abstract class BaseEntity(
        var id: Long? = null,
        var deleteStatus: Int? = 0/*,
        @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss") private var createTime: Date? = null,
        @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss") private var updateTime: Date? = null*/
) : Serializable
