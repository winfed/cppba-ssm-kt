package com.cppba.dto

import com.cppba.entity.User
import javax.persistence.Entity

@Entity
class UserDto(
        var cityName: String = ""
) : User()
