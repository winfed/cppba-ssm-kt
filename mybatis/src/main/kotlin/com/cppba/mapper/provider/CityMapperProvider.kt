package com.cppba.mapper.provider

// 动态生成sql
class CityMapperProvider {

	private val BASE_SELECT :String = "c.id, c.delete_status, c.name, c.parent_id"

	fun findByParentId(parentId: Long?): String {
		 var sql = " SELECT $BASE_SELECT FROM city c  where c.delete_status = 0 "
		if (parentId != null) {
			sql += " and c.parent_id = #{parentId} "
		}
		return sql
	}
}
