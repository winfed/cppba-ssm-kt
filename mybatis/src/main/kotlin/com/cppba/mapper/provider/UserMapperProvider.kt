package com.cppba.mapper.provider

import org.apache.commons.lang3.StringUtils

// 动态生成sql
class UserMapperProvider {

    private val BASE_SELECT = " u.id, u.delete_status, u.username, u.password, u.city_id, u.sex "


    fun getById(id: Long): String {
        var sql = "SELECT $BASE_SELECT, c.name as cityName FROM user u LEFT JOIN city c on u.city_id = c.id where u.id = #{id} "
        return sql
    }

    fun page(username: String?): String {
        var sql = " SELECT $BASE_SELECT, c.name as cityName FROM user u LEFT JOIN city c on u.city_id = c.id where u.delete_status = 0 "
        if (StringUtils.isNotBlank(username)) {
            sql += " and u.username LIKE concat('%',#{username},'%') "
        }
        return sql
    }
}
