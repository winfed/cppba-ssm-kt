package com.cppba.mapper

import com.cppba.dto.UserDto
import com.cppba.entity.User
import com.cppba.mapper.provider.UserMapperProvider
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.SelectProvider

@Mapper
interface UserMapper : tk.mybatis.mapper.common.Mapper<User> {

    /*@Results(id = "UserDtoResultMap",
            value = {
                    //只需要写数据库名称和实体名称不相同的，如果相同会自动映射
                    @Result(column = "name", property = "cityName", javaType = String.class)
            }
    )*/
    @SelectProvider(type = UserMapperProvider::class, method = "getById")
    fun getById(id: Long): UserDto

    // @ResultMap("UserDtoResultMap")
    @SelectProvider(type = UserMapperProvider::class, method = "page")
    fun page(username: String?): List<UserDto>
}
