package com.cppba.mapper

import com.cppba.entity.City
import com.cppba.mapper.provider.CityMapperProvider
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.SelectProvider


/**
 * @author zwf
 * @created 2017/7/6 0006 15:59
 */
@Mapper
interface CityMapper : tk.mybatis.mapper.common.Mapper<City> {

    @SelectProvider(type = CityMapperProvider::class, method = "findByParentId")
    fun findByParentId(parentId: Long): List<City>

}
