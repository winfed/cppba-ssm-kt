/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : mybatis

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-11-09 17:45:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delete_status` int(11) DEFAULT '0' COMMENT '删除状态0：正常，1：删除',
  `name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '0', '重庆', '0', '2017-06-21 17:49:44', '2017-11-09 11:43:12');
INSERT INTO `city` VALUES ('2', '0', '渝北区', '4', '2017-06-22 17:49:44', '2017-11-09 11:43:12');
INSERT INTO `city` VALUES ('3', '0', '渝中区', '4', '2017-06-23 17:49:44', '2017-11-09 11:43:12');
INSERT INTO `city` VALUES ('4', '0', '重庆市', '1', '2017-06-29 11:12:15', '2017-11-09 11:43:12');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delete_status` int(11) DEFAULT '0' COMMENT '删除状态0：正常，1：删除',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '插入时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '0', 'xiaoming', '123', '2', '1', '2017-06-21 11:49:44', '2017-11-09 11:41:37');
INSERT INTO `user` VALUES ('2', '0', 'xiaohong', '456', '3', '2', '2017-06-22 12:49:44', '2017-11-09 11:41:37');
INSERT INTO `user` VALUES ('3', '0', 'xiaogang', '234', '2', '1', '2017-06-23 13:49:44', '2017-11-09 11:41:37');
INSERT INTO `user` VALUES ('4', '0', 'name', 'pass', '2', '2', '2017-06-30 17:35:59', '2017-11-09 11:41:37');
INSERT INTO `user` VALUES ('5', '0', 'name', 'pass', '2', '2', '2017-06-30 17:37:29', '2017-11-09 11:41:37');
INSERT INTO `user` VALUES ('6', '0', 'name', 'pass', '2', '2', '2017-06-30 17:37:50', '2017-11-09 11:41:37');
